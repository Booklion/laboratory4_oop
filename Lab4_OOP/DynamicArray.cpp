#include "DynamicArray.h"
#include<iostream>
#include<fstream>
using namespace std;

DynamicArray::DynamicArray() {
	len = 0;
	cap = 10;
	data = new void*[cap];
}


DynamicArray::DynamicArray(const DynamicArray& other) {
	data = new void*[other.cap];
	this->len = other.len;
	this->cap = other.cap;
	for (int i = 0; i < this->len; i++) {
		//memcpy(other.data, data, sizeof(void**) * len);
		this->data[i] = other.data[i];
	}
}


void DynamicArray::insert(void* elem) {
	if (this->len == this->cap) {
		this->cap = this->cap * 2;
		void** newArray = new void* [this->cap];
		for (int i = 0; i < this->len; i++) {
			newArray[i] = this->data[i];
		}
		delete[] this->data;
		this->data = nullptr;
		this->data = newArray;
	}
	
	this->data[this->len++] = elem;
}


void DynamicArray::deleteLast() {
	delete &this->data[this->len - 1];
}


int DynamicArray::getCapacity() {
	return this->cap;
}


int DynamicArray::getLength() {
	return this->len;
}


void DynamicArray::setCapacity(int newCapacity) {
	cap = newCapacity;
}


void DynamicArray::setLength(int newLength) {
	len = newLength;
}


void DynamicArray::display(void (*displayElement)(void**)) {
	for (int i = 0; i < this->len; i++) {
		displayElement(&this->data[i]);
	}
	cout << endl;
}


void DynamicArray::release() {
	for (int i = 0; i < this->len; i++) {
		delete &this->data[i];
	}
	delete[] this->data;
	this->data = nullptr;
}


int DynamicArray::search(bool (*condition)(void**)) {
	for (int i = 0; i < this->len; i++) {
		if (condition(&this->data[i])) {
			return i;
		}
	}
	return -1;
}


ostream& operator<<(ostream& os, const DynamicArray& arr){
	for (int i = 0; i < arr.len; i++) {
		os << arr.data[i] << ", ";
	}
	return os;
}


istream& operator>>(istream& is, const DynamicArray& array) {
	int a;
	is >> a;

	is >> a;
	
	return is;
}


void* DynamicArray::operator[](int i) {
	return this->data[i];
}

void* DynamicArray::accumulate(void* (*addition)(void* number1, void* number2), void* (*initialise)()) {
	void* s = initialise();
	for (int i = 0; i < this->len; i++) {
		s = addition(s,this->data[i]);                  
	}
	return s;
}
