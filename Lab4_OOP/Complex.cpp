#include "Complex.h"
#include<math.h>

Complex::Complex(){
	m_real = 0;
	m_imaginary = 0;
}

Complex::Complex(float real, float imaginary){ 
	m_real = real;
	m_imaginary = imaginary;
}

Complex Complex::addition(Complex z){
	Complex result{this->m_real + z.m_real, m_imaginary + z.m_imaginary};
	return result;
}

Complex Complex::conjugate(Complex z) {
	Complex result;
	result.m_real = z.m_real;
	result.m_imaginary = z.m_imaginary;
	return result;
}

bool Complex::operator==(const Complex& z) const {
	return (this->m_real == z.m_real && this->m_imaginary == z.m_imaginary);
}

bool Complex::operator!=(const Complex& z) const {
	return (this->m_real != z.m_real || this->m_imaginary != z.m_imaginary);
}

Complex Complex::operator+(const Complex &z) {
	Complex result{ this->m_real + z.m_real, m_imaginary + z.m_imaginary };
	return result;
}

Complex Complex::operator-(const Complex& z){
	Complex result{ this->m_real - z.m_real, this->m_imaginary - z.m_imaginary };
	return result;
}

Complex Complex::operator*(const Complex& z) {
	Complex z1, result;
	result.m_real = (this->m_real * z.m_real) - (this->m_imaginary * z.m_imaginary);
	result.m_imaginary = (this->m_real * z.m_imaginary) + (this->m_imaginary * z.m_real);
	return result;
}

Complex Complex::operator/(const Complex& z) {
	Complex result;
	float denominator;
	denominator = float(pow(z.m_real, 2)) + float(pow(z.m_imaginary, 2));
	result.m_real = float(((this->m_real * z.m_real) - (this->m_imaginary * z.m_imaginary))) / denominator;
	result.m_imaginary = float(((this->m_real * z.m_imaginary) + (this->m_imaginary * z.m_real))) / denominator;
	return result;
}

void Complex::setReal(float r){
	m_real = r;
}

void Complex::setImag(float i){
	m_imaginary = i;
}

ostream& operator<<(ostream& os, const Complex& c){
	os << c.m_real << " + " << c.m_imaginary << "i" << endl;
	return os;
}

istream& operator>>(istream& is, Complex& c){
	float n;
	is >> n;
	c.setReal(n);
	is >> n;
	c.setImag(n);
	return is;
}