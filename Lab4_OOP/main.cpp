#include<iostream>
#include "Complex.h"
#include "DynamicArray.h"
using namespace std;

void printInt(void* a) {
	int* ai = (int*)a;
	cout << *ai << " ";
}

void printFloat(void* a) {
	float* ai = (float*)a;
	cout << *ai << " ";
}

void printComplex(void* a) {
	Complex* ai = (Complex*)a;
	cout << *ai << " ";
}

void* initialise() {
	Complex* z0 = new Complex;
	return (void*)z0;
}



void* addComplex(void* x, void* y) {
	Complex* z1 = (Complex*)x;
	Complex* z2 = (Complex*)y;
	Complex* result = new Complex;
	*result = z1->addition(*z2);
	return (void*)result;
}


int main()
{
	Complex a{10, 10}, b(2, 4);
	Complex c{ -1, 10 };        //new way of working with classes in C++
	Complex d = a.addition(b);  //to a, we added b, and we stored the result in d

	Complex z;
	z = a + b;
	z = a.operator+(b);
	cout << z << endl;

	cout << "Enter the real and the imaginary components of the complex number: "; cin >> z;
	cout << "After reading, z is: " << z << endl;


	DynamicArray arr1;

	arr1.insert((void**)1);
	arr1.insert((void**)2);

	DynamicArray arr2 = arr1;
	DynamicArray arr3;

	arr3 = arr2;
	//*arr2[0] = (void**)10;
	//*arr3[1] = (void**)7;

	cout << "The first array is: " << arr1 << endl;
	cout << "The second array is: " << arr2 << endl;
	cout << "The third array is: " << arr3 << endl;


	ifstream f;
	f.open("D:\\#Visual Studio Code\\source\\repos\\OOP\\Lab4_OOP\\complex_numbers.txt");
	if (! f.is_open()) {
		exit(-1);
	}

	DynamicArray fileArray, fileArrayReal, fileArrayImaginary;
	DynamicArray complexFileArray;
	float* y = new float;
	Complex* fileSum;
	int i = 0;
	while (true) {
		float* y = new float;
		if (!(f >> *y))
			break;
		//cout << *y << endl;
		fileArray.insert((void*)y);
		//cout << "!!!!" << *(float*)fileArray[i] << endl;;
		
		i++;
	}

	for (int i = 0; i < fileArray.getLength(); i++) {

		cout << *(float*)fileArray[i] << endl;
		if (i % 2 == 1) {
			fileArrayReal.insert(fileArray[i]);
		}
		else {
			fileArrayImaginary.insert(fileArray[i]);
		}



	}

	

	for (int i = 0; i < fileArrayReal.getLength(); i++) {
		
		Complex* c = new Complex{*(float*) fileArrayReal[i], *(float*)fileArrayImaginary[i]};
		complexFileArray.insert((void*)c);
		printComplex(complexFileArray[i]);
	}
	fileSum = (Complex*)complexFileArray.accumulate(addComplex, initialise);

	cout << "The sum of the elements in the file is: " << *fileSum << endl;

	return 0;
}
