#pragma once
#include <ostream>
#include <istream>
using namespace std;

class Complex{
public:
	//Default constructor
	Complex();

	//Constructor that initializes a complex number with the real and imaginary parts
	Complex(float real, float imaginary);

	//We add the number z to the current object; it's linekd to an object
	Complex addition(Complex z);

	//Computes the conjugate of a complex number
	Complex conjugate(Complex z);

	//Overload for the equal to operator (==)
	bool operator==(const Complex& z) const;

	//Overload for the not equal to operator (!=)
	bool operator!=(const Complex& z) const;

	//Overload for the addition operator (+)
	Complex operator+(const Complex &c);

	//Overload for the subtraction operator (-)
	Complex operator-(const Complex& z);

	//Overload for the multiplication operator (*)
	Complex operator*(const Complex& z);

	//Overload for the division operator (/)
	Complex operator/(const Complex& z);

	//Setter for the real part of the number
	void setReal(float r);

	//Setter for the imaginary part of the number
	void setImag(float i);

	//Overload for the stream extraction operator (<<)
	friend ostream& operator<<(ostream& os, const Complex& c);

private:
	float m_real;
	float m_imaginary;
};

//Overload for the stream insertion operator (>>)
istream& operator>>(istream& is, Complex& c);

