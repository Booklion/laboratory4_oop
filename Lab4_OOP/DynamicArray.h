#pragma once
#include "Complex.h"
#include<ostream>
#include<istream>
#include<fstream>
using namespace std;

class DynamicArray {
public:
	//Default constructor
	DynamicArray();

	//Copy constructor
	DynamicArray(const DynamicArray& other);

	//Insert the element at the end of the array
	void insert(void* elem);

	//Remove the last element of the array
	void deleteLast();

	//Getter for the capacity of the array
	int getCapacity();

	//Getter for the length of the array
	int getLength();

	//Setter for the capacity of the array
	void setCapacity(int newCapacity);

	//Setter for the length of the array
	void setLength(int newLength);

	//Dsiplays the element that fulfills a certain criterion (given by (*displayElement))
	void display(void (*displayElement)(void**));

	//Destructor
	void release();  

	//Searches for an element in the dynamic array that fulfills a certain condition (given by (*condition))
	int search(bool (*condition)(void**));

	//Overload for the stream extraction operator (<<)
	friend ostream& operator<<(ostream& os, const DynamicArray& arr);

	//Overload for the indexing/subscript operator ([])
	//Returns a reference to the i-th element in the array
	void* operator[](int i);

	//Computes the sum of the elements in the array
	void* accumulate(void* (*addition)(void* number1, void* number2), void* (*initialise)());

private:
	int len;
	int cap;
	void** data;
};

//Overload for the stream insertion operator (>>)
istream& operator>>(istream& is, const DynamicArray& arr);
